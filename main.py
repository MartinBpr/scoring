import numpy as np
from sklearn.tree import DecisionTreeClassifier
import data_handler
import tools
from learner import BoostingLearner, WeakLeaner
import metrics
import matplotlib.pyplot as plt
import pandas as pd

np.random.seed(1)

path = '/Users/Martin/Projects/Scoring/'
filename = 'data2b.csv'
file_path = '%s/%s' % (path, filename)

Y_train, X_train, Y_test, X_test = data_handler.get_data(file_path)


# INITIALIZATION
n = len(X_train)
Y_train_nd = tools.to_ndarray(Y_train)
W = np.ones(n) / n

train_AUCs = []
test_AUCs = []

F = BoostingLearner()

# we will update this with data for logistic regression
compare_logistic = True
if compare_logistic:
    data_logistic_train = pd.DataFrame()
    data_logistic_train['labels'] = Y_train_nd
    data_logistic_test = pd.DataFrame()
    data_logistic_test['labels'] = tools.to_ndarray(Y_test)


# ALGORITHM
print '\n\n'


print_AUC = True
for t in range(200):
    best_loss = np.inf
    for feature in X_train.columns:
        x = X_train[feature].values
        x = np.array([x]).T

        #remove nan lines
        nan_lines = np.isnan(x).any(axis=1)
        x = x[~nan_lines]
        y = Y_train_nd[~nan_lines]
        w = W[~nan_lines]

        tree_clf = DecisionTreeClassifier(criterion='gini', splitter='best', max_depth=1)
        tree_clf = tree_clf.fit(x, y, sample_weight=w)

        # score is the mean weigthed accuracy
        loss = - tree_clf.score(x, y, sample_weight=w)
        wl = WeakLeaner(tree_clf, feature)

        if loss < best_loss:
            h = wl
            best_loss = loss

    ht = h.predict_proba(X_train)


    f = np.log(np.divide(ht, 1.-ht))*0.5
    adjust = np.exp(np.multiply(-Y_train_nd, f))
    W = np.multiply(adjust, W)
    W = W / sum(W)
    F.learners += [h]
    threshold = h.tree.tree_.threshold[0]
    print t, h.col_name, threshold

    score_train = F.score(X_train)
    score_train = (score_train - min(score_train))/(max(score_train) - min(score_train))

    train_AUC = metrics.get_AUC(F, X_train, Y_train, n_bootstrap=0)
    test_AUC = metrics.get_AUC(F, X_test, Y_test, n_bootstrap=0)

    train_AUCs += [train_AUC]
    test_AUCs += [test_AUC]

    if print_AUC:
        print 'train AUC\t', train_AUC
        print 'test AUC \t', test_AUC
        print '-------------'

    if len(test_AUCs) > 15 and max(test_AUCs[-15:]) < max(test_AUCs[:-15]):
        best_t = test_AUCs.index(max(test_AUCs))
        F.learners = F.learners[:best_t+1]
        break

    # Update logistic data
    if compare_logistic:
        train_logistic = np.copy(X_train[feature].values)
        train_logistic[X_train[feature].values >= threshold] = 1
        train_logistic[X_train[feature].values < threshold] = 0

        data_logistic_train['col%s_%i' % (h.col_name, t)] = train_logistic

        test_logistic = np.copy(X_test[feature].values)
        test_logistic[X_test[feature].values >= threshold] = 1
        test_logistic[X_test[feature].values < threshold] = 0

        data_logistic_test['col%s_%i' % (h.col_name, t)] = test_logistic


ax1 = plt.subplot(211)
ax1.plot(train_AUCs, label='AUC on train set')
ax1.plot(test_AUCs, label='AUC on test set')
ax1.legend(loc=4)
ax1.set_xlabel('iterations')
ax1.set_ylabel('AUC')
ax1.set_title('AUC over iterations')

ax2 = plt.subplot(223)
metrics.ROC_Curve(F, X_train, Y_train, n_bootstrap=10, ax=ax2, title_pref='Train : ')

ax3 = plt.subplot(224)
metrics.ROC_Curve(F, X_test, Y_test, n_bootstrap=10, ax=ax3, title_pref='Test : ')
plt.subplots_adjust(hspace=0.35)
plt.show()
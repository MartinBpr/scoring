from sklearn.feature_extraction import DictVectorizer
import re
import pandas as pd

def replace_comma(row):
    try:
        return re.sub(r"^(?P<name>\d+)?,(?P<name2>\d+)", r'\1.\2', row)
    except:
        return row


def one_hot_dataframe(data, cols, replace=False, avoid_colinearity=True):
    """ Takes a dataframe and a list of columns that need to be encoded.
        Returns a 3-tuple comprising the data, the vectorized data,
        and the fitted vectorizor.
    """
    vec = DictVectorizer()
    mkdict = lambda row: {cols[i]:row[i] for i in range(len(cols))}
    vecData = pd.DataFrame(vec.fit_transform(map(mkdict, data[cols].values)).toarray())
    vecData.columns = vec.get_feature_names()
    vecData.index = data.index



    cols_to_remove = set(cols)
    to_keep_cols = []
    for f in vec.get_feature_names():
        f_base_name = f.split("=")[0]
        if f.split("=")[0] in cols_to_remove and avoid_colinearity:
            cols_to_remove.remove(f_base_name)
        else:
            to_keep_cols += [f]

    if replace:
        data = data.drop(cols, axis=1)
        data = data.join(vecData[to_keep_cols])
        return data
    else :
        return vecData[to_keep_cols]


def to_ndarray(serie):
    return serie.values[:, 0]

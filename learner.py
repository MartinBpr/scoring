import numpy as np


class WeakLeaner():
    epsilon = 1e-2

    def __init__(self, tree, col_name):
        self.tree = tree
        self.col_name = col_name

    def predict_proba(self, x):
        if x.__class__.__name__ == 'DataFrame':
            x = x[self.col_name].values
            x = np.array([x]).T

        nan_lines = np.isnan(x).any(axis=1)
        x_nan = x[nan_lines]
        x_finite = x[~nan_lines]

        probas = np.zeros(x.shape[0])
        probas[~nan_lines] = self.tree.predict_proba(x_finite)[:, 1]
        probas[nan_lines] = 0.5
        return probas

    def classify(self, x):
        proba = self.predict_proba(x)
        preds = np.zeros_like(x)
        preds[proba > 0.5] = 1
        preds[proba <= 0.5] = -1
        return preds


class BoostingLearner():
    # used in order to avoid proba = 1 or 0
    EPSILON = 1e-4
    def __init__(self):
        self.learners = []

    def score(self, x):
        score = np.zeros(len(x))
        for h in self.learners:
            ht = h.predict_proba(x)
            ht = np.minimum(ht, 1 - BoostingLearner.EPSILON)
            ht = np.maximum(ht, BoostingLearner.EPSILON)
            f = np.log(np.divide(ht, 1.-ht))*0.5
            score += f
        return score

    # this is a fake proba used for ROC curve
    # ROC curve is stable by any affine transformation (with positive slope)
    # we just normalize it between 0 and 1
    def predict_proba(self, x):
        score = self.score(x)
        proba = (score - min(score))/(max(score - min(score)))
        return proba

    def classify(self, x):
        proba = self.score(x)
        preds = np.zeros(len(x))
        preds[proba > 0] = 1
        preds[proba <= 0] = -1
        return preds
import numpy as np
import pandas as pd
import random
import tools

np.random.seed(1234)


dep_var = ['defaut_1an']
id_var = ['id_operation', 'annee']
num_var = ['var_signa4bis', 'var_signa6', 'var_comportement1',  'var_comportement2', 'montant1', 'montant3', 'montant4','ratio1','ratio2']
cat_var = ['var_signa1', 'group.profession', 'var_comportement3', 'var_octroi1', 'var_octroi2']
useless_var = ['var_signa2', 'var_signa3', 'var_signa4', 'var_signa5', 'var_octroi3', 'var_octroi4', 'montant2',  'sain_a_date', 'var_signa22', 'date_entree_defaut', 'date_entree_defaut2']
expl_var = num_var + cat_var


def import_data(file_path):
    df = pd.read_csv(file_path, sep=';')

    # check we have not forgot anything
    assert set(df.columns) == set(dep_var + id_var + num_var + cat_var + useless_var + expl_var)

    df = df[dep_var + expl_var]

    return df


def treat_data(df):
    # Dep var : -1 default, 1 no def
    df['defaut_1an'] = df['defaut_1an'].replace(1, -1)
    df['defaut_1an'] = df['defaut_1an'].replace(0, 1)

    # treat numeric variables
    df[num_var] = df[num_var].applymap(tools.replace_comma)
    df[num_var] = df[num_var].astype(float)

    # treat categorical variables

    # quick fix
    df['var_octroi1'] = df['var_octroi1'].replace('Manquant', np.nan)
    df['var_signa1'] = df['var_signa1'].replace('manquant', np.nan)

    df = tools.one_hot_dataframe(df, cat_var, replace=True)


    return df


def split_data(df, verbose=True):
    train_prop = 0.8

    # Training sample : 80% data
    train_index = random.sample(df.index, int(train_prop * len(df)))
    df_train = df.ix[train_index]

    # Test sample : 20% data
    df_test = df.drop(train_index)

    Y_train = df_train[dep_var]
    X_train = pd.DataFrame(df_train.drop(dep_var, axis=1))

    Y_test = df_test[dep_var]
    X_test = pd.DataFrame(df_test.drop(dep_var, axis=1))

    print int(train_prop*100), len(Y_train), sum(Y_train[dep_var].values == 1), float(sum(Y_train[dep_var].values == 1))/len(Y_train)
    if verbose:
        print '%i%% in train set : %i observations with %i positives (%i%%)' \
              % (int(train_prop*100), len(Y_train), sum(Y_train[dep_var].values == 1)[0],
                 int(round(100*float(sum(Y_train[dep_var].values == 1))/len(Y_train))))
        print '%i%% in test set : %i observations with %i positives (%i%%)' \
              % (int(round(((1-train_prop)*100))), len(Y_test), sum(Y_test[dep_var].values == 1)[0],
                 int(round(100*float(sum(Y_test[dep_var].values == 1))/len(Y_test))))

    return Y_train, X_train, Y_test, X_test


def get_data(file_path):
    df = import_data(file_path)
    df = treat_data(df)
    Y_train, X_train, Y_test, X_test = split_data(df)
    return Y_train, X_train, Y_test, X_test



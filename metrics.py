from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
import numpy as np


def ROC_Curve(model, x_test, y_test, ax=None, n_bootstrap=10, title_pref=''):

    if not ax :
        ax = plt.subplot(111)
    ax.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    
    roc_auc_list = []

    for i in range(n_bootstrap):
        sample_index = np.random.choice(x_test.index, len(x_test), replace=True)
        x_sample = x_test.ix[sample_index]
        y_sample = y_test.ix[sample_index]

        probas_ = model.predict_proba(x_sample)
        
        fpr, tpr, _ = roc_curve(y_sample, probas_)
        
        roc_auc_list += [auc(fpr, tpr)]
        ax.plot(fpr, tpr)
    
    mean_auc = sum(roc_auc_list)/len(roc_auc_list)
    ax.set_title('%sROC curve (area = %0.4f)' % (title_pref, mean_auc))
    
    

def get_AUC(model, x_test, y_test, n_bootstrap=10):
    
    roc_auc_list = []
    if n_bootstrap == 0:
        probas_ = model.predict_proba(x_test)
        fpr, tpr, _ = roc_curve(y_test, probas_)
        mean_auc = auc(fpr, tpr)

    else:
        for i in range(n_bootstrap):
            sample_index = np.random.choice(x_test.index, len(x_test), replace=True)
            x_sample = x_test.ix[sample_index]
            y_sample = y_test.ix[sample_index]

            probas_ = model.predict_proba(x_sample)

            fpr, tpr, _ = roc_curve(y_sample, probas_)

            roc_auc_list += [auc(fpr, tpr)]

        mean_auc = sum(roc_auc_list)/len(roc_auc_list)
    return(mean_auc)